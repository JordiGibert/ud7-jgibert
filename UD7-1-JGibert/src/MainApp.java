import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

public class MainApp {
	static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {
		Hashtable<String,Double> dades=new Hashtable<String,Double>();
		// TODO Auto-generated method stub
		System.out.println("RECORDA: Per a tancar el programa i mostrar les dades introdueix 'sortir' quant et pregunti el nom del alumne");
		boolean sortir=false;
		do {
		System.out.print("Introdueix el codi del alumne: ");
		String clau=scan.next();
		if(clau.equals("sortir")) {
			System.out.println("Mostrant notes:");
			sortir=true;
		} else {
			Double mitjana=calcularMitjana();
			dades.put(clau, mitjana);
		}
		}while(sortir==false);
		Enumeration<String> claus=dades.keys();
		while(claus.hasMoreElements()) {
			String tempClau= claus.nextElement();
			Double tempMitjana=dades.get(tempClau);
			System.out.println("\tMitjana de "+tempClau+": "+tempMitjana);
		}
	}

	public static Double calcularMitjana() {
		boolean acabat=false;
		double mitjana=0.0;
		int i=0;
		System.out.println("\tRECORDA: Per a deixar de posar notes introdueix un 0");
		do {
			System.out.print("\tIntrodueix la nota numero "+(i+1)+": ");
			Double nota=scan.nextDouble();
			if(nota==0.0) {
				acabat=true;
			} else {
				mitjana=mitjana+nota;
				i++;
			}
		} while(acabat==false);
		mitjana=mitjana/i;
		System.out.println(mitjana);
		return mitjana;
	}
}
