import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

public class MainApp {
	static Scanner scan = new Scanner(System.in);
	static Hashtable<String, Double> cataleg=new Hashtable<String, Double>();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		generarCataleg();
		int menu=0;
		do {
			menu=menu();
			switch(menu) {
				case 1:
					afegirArticle();
					break;
				case 2:
					buscarArticle();
					break;
				case 3:
					llistarCataleg();
					break;
			}
		} while(menu!=4);
		System.out.println("Adeu");
	}
	
	private static void llistarCataleg() {
		Enumeration<String> noms = cataleg.keys();
		Enumeration<Double> preus = cataleg.elements();
		while(noms.hasMoreElements()) {
			System.out.println(noms.nextElement()+": "+preus.nextElement()+"�");
		}
	}
	
	private static void buscarArticle() {
		System.out.println("Introdueix el nom del producte que busques: ");
		String buscar=scan.next();
		if(cataleg.get(buscar) != null) {
			System.out.println("Producte: "+buscar+"\tPreu: "+cataleg.get(buscar));
		} else {
			System.out.println(buscar+" no existeix en el nostre cataleg");
		}
		
	}
	
	private static void afegirArticle() {
		System.out.print("Nom del article: ");
		String clau=scan.next();
		System.out.print("Preu del article: ");
		Double preu=scan.nextDouble();
		cataleg.put(clau, preu);
		System.out.println("Article afegit!");
	}
	
private static void generarCataleg() {
	cataleg.put("lleixum", 1.2);
	cataleg.put("mengar lloro", 2.2);
	cataleg.put("cereals", 3.95);
	cataleg.put("paquet llet", 4.2);
	cataleg.put("bistec", 5.2);
	cataleg.put("pernil iberic", 6.8);
	cataleg.put("yogurt premium", 7.8);
	cataleg.put("lasanya iberica", 8.8);
	cataleg.put("colonia", 9.8);
	cataleg.put("paquet bulquers", 10.8);
}

private static int menu() {
	System.out.println("Que vol fer?"
			+ "\n1- Afegir article"
			+ "\n2- Buscar article"
			+ "\n3- Llistar articles"
			+ "\n4- Sortir del programa");
	int menu=0;
		menu=scan.nextInt();
		while(menu<=0 || menu>=5){
		System.out.println("Valor incorrecte");
		menu=scan.nextInt();
		}
		return menu;
}
}
