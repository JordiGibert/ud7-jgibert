import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Scanner;

public class MainApp {
	static Scanner scan = new Scanner(System.in);
	static Hashtable<String, Double> cataleg=new Hashtable<String, Double>();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		generarCataleg();
		int menu=0;
		do {
			menu=menu();
			switch(menu) {
				case 1:
					afegirArticle();
					break;
				case 2:
					buscarArticle();
					break;
				case 3:
					llistarCataleg();
					break;
				case 4:
					simularCompra();
					break;
			}
		} while(menu!=5);
		System.out.println("Adeu");
	}
	
	private static void simularCompra() {
		//ArrayList<Double> carrito = new ArrayList<>();
		System.out.println("RECORDA, per anar al caixer introdueix un 0");
		boolean caixer=false;
		ArrayList<String> carrito=buscarArticleCompra();
		
		
		
		//Caixa de la compra
		System.out.println("Benvingut a la caixa!");
		ArrayList<Double> preuCarrito=arrayPreus(carrito);
		//Generar array de IVA
		Iterator<Double> it= preuCarrito.iterator();
		int i=0;
		ArrayList<Double> carritoIVA = new ArrayList<>();
		while(it.hasNext()) {
			Double caixa=it.next();
			//IVA
			boolean IVABe=false;
			Double IVA= 4.0;
			do {
				System.out.println("El producte "+carrito.get(i)+" amb el preu "+caixa+" te un IVA del 21% o del 4%?");
				IVA=scan.nextDouble();
				if(IVA==21.0 || IVA==4.0) {
				 IVABe=true;
				} else {
					System.out.println("El valor introduit no es ni 4 ni 21");
				}
			} while(IVABe==false);
			//calcular IVA
			carritoIVA.add(caixa*(IVA/100));
			i++;
		}
		//calcular total brut i iva
		Double totalBrut=sumaArray(preuCarrito);
		Double totalIVA=sumaArray(carritoIVA)+totalBrut;
		//mostrar preus
		System.out.println("La teva compra de "+carrito.size()+" productes te un total brut de: "+totalBrut);
		System.out.println("El total de la teva compra es: "+totalIVA);
		System.out.println("Quant vols pagar?");
		Double pagar=scan.nextDouble();
		while(totalIVA>pagar) {
			System.out.println("Quantitat insuficient, introdueixna una de nova: ");
			pagar=scan.nextDouble();
		}
		System.out.println("El canvi es: "+(pagar-totalIVA));
	}
	
	private static ArrayList<Double> arrayPreus(ArrayList<String> carrito) {
		Iterator<String> it= carrito.iterator();
		ArrayList<Double> preus= new ArrayList<Double>();
		while(it.hasNext()) {
			preus.add(cataleg.get(it.next()));
		}
		return preus;
		
	}
	private static void llistarCataleg() {
		Enumeration<String> noms = cataleg.keys();
		Enumeration<Double> preus = cataleg.elements();
		while(noms.hasMoreElements()) {
			System.out.println(noms.nextElement()+": "+preus.nextElement()+"�");
		}
	}
	
	private static void buscarArticle() {
		System.out.println("Introdueix el nom del producte que busques: ");
		String buscar=scan.next();
		if(cataleg.get(buscar) != null) {
			System.out.println("Producte: "+buscar+"\tPreu: "+cataleg.get(buscar));
		} else {
			System.out.println(buscar+" no existeix en el nostre cataleg");
		}
		
	}
	
	private static ArrayList<String> buscarArticleCompra() {
		ArrayList<String> carrito = new ArrayList<String>();
		System.out.println("RECORDA: Per acabar de comprar i anar a la caixa, escriu 'caixa'");
		String buscar=null;
		int i=1;
		do {
			System.out.print("Introdueix el nom del producte num "+i+": ");
			buscar=scan.next();
			if(!buscar.equals("caixa")) {
				while(cataleg.get(buscar) == null && !buscar.equals("caixa")){
					System.out.print(buscar+" no existeix en el nostre cataleg, torna a introduir el nom: ");
					buscar=scan.next();
				}
				carrito.add(buscar);
				i++;
			}
		} while(!buscar.equals("caixa"));
		
		return carrito;
		
	}
	
	private static void afegirArticle() {
		System.out.print("Nom del article: ");
		String clau=scan.next();
		System.out.print("Preu del article: ");
		Double preu=scan.nextDouble();
		cataleg.put(clau, preu);
		System.out.println("Article afegit!");
	}
	
private static void generarCataleg() {
	cataleg.put("lleixum", 1.2);
	cataleg.put("mengar lloro", 2.2);
	cataleg.put("cereals", 3.95);
	cataleg.put("paquet llet", 4.2);
	cataleg.put("bistec", 5.2);
	cataleg.put("pernil iberic", 6.8);
	cataleg.put("yogurt premium", 7.8);
	cataleg.put("lasanya iberica", 8.8);
	cataleg.put("colonia", 9.8);
	cataleg.put("paquet bulquers", 10.8);
}

private static int menu() {
	System.out.println("Que vol fer?"
			+ "\n1- Afegir article"
			+ "\n2- Buscar article"
			+ "\n3- Llistar articles"
			+ "\n4- Simular compra"
			+ "\n5- Sortir del programa");
	int menu=0;
		menu=scan.nextInt();
		while(menu<=0 || menu>=6){
		System.out.println("Valor incorrecte");
		menu=scan.nextInt();
		}
		return menu;
}

private static Double sumaArray(ArrayList<Double> array) {
	Iterator<Double> it= array.iterator();
	Double total=0.0;
	while(it.hasNext()) {
		total=total+it.next();
	}
	return total;
	
}
}

