import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class MainApp {
	static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {
		ArrayList<Double> carrito = new ArrayList<>();
		System.out.println("RECORDA, per anar al caixer introdueix un 0");
		int i=0;
		boolean caixer=false;
		//Introduir preus productes
		do {
			System.out.print("Introdueix el preu del producte num "+(i+1)+": ");
			Double preuProducte=scan.nextDouble();
			if(preuProducte==0.0) {
				System.out.println("Cobrant en caixa...");
				caixer=true;
			} else {
				carrito.add(preuProducte);
				i++;
			}
		}while(caixer==false);
		//Caixa de la compra
		System.out.println("Benvingut a la caixa!");
		//Generar array de IVA
		Iterator<Double> it= carrito.iterator();
		i=1;
		ArrayList<Double> carritoIVA = new ArrayList<>();
		while(it.hasNext()) {
			Double caixa=it.next();
			//IVA
			boolean IVABe=false;
			Double IVA= 4.0;
			do {
				System.out.println("El producte num "+i+" amb el preu "+caixa+" te un IVA del 21% o del 4%?");
				IVA=scan.nextDouble();
				if(IVA==21.0 || IVA==4.0) {
				 IVABe=true;
				} else {
					System.out.println("El valor introduit no es ni 4 ni 21");
				}
			} while(IVABe==false);
			//calcular IVA
			carritoIVA.add(caixa*(IVA/100));
			i++;
		}
		//calcular total brut i iva
		Double totalBrut=sumaArray(carrito);
		Double totalIVA=sumaArray(carritoIVA)+totalBrut;
		//mostrar preus
		System.out.println("La teva compra de "+carrito.size()+" productes te un total brut de: "+totalBrut);
		System.out.println("El total de la teva compra es: "+totalIVA);
		System.out.println("Quant vols pagar?");
		Double pagar=scan.nextDouble();
		while(totalIVA>pagar) {
			System.out.println("Quantitat insuficient, introdueixna una de nova: ");
			pagar=scan.nextDouble();
		}
		System.out.println("El canvi es: "+(pagar-totalIVA));
		
		
	}
	
	private static Double sumaArray(ArrayList<Double> array) {
		Iterator<Double> it= array.iterator();
		Double total=0.0;
		while(it.hasNext()) {
			total=total+it.next();
		}
		return total;
		
	}
}
